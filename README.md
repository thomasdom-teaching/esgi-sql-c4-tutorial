# SQL Querying @ ESGI - Course 4 - Tutorial

## How to use

### Prerequisites

You need to have `Docker Engine` ([Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/), [OSX](https://docs.docker.com/docker-for-mac/install/) or [Windows](https://docs.docker.com/docker-for-windows/install/)) and [Docker Compose](https://docs.docker.com/compose/install/) installed on your computer.

You can also use `make` to launch commands faster but it's not mandatory.

### Run the database

`make run` OR `docker-compose up`

### Access the database shell

`make shell` OR `docker-compose exec db bash`

Then:

`psql`

### Start the database in the background

`make start` OR `docker-compose up -d`

### Stop the database when started in background

`make stop` OR `docker-compose stop`

### Remove database containers and network

`make destroy` OR `docker-compose down`

### Erase all data (WARNING: THIS IS IRREVERSIBLE)

`make dangerously-clean` for help on how you can erase all data

OR (dangerous):

```shell
docker-compose down --rmi all -v
rm -rf ./.docker/postgresql/*
touch ./.docker/postgresql/.gitkeep
```

Enjoy!
